CREATE DATABASE dream_shop;

USE dream_shop;

CREATE TABLE `items` (
	`id_item` INT(11) UNSIGNED NOT NULL,
	`name` VARCHAR(100) NULL DEFAULT NULL,
	`cost` DECIMAL(5,2) UNSIGNED NOT NULL DEFAULT '0.00',
	`stock` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	PRIMARY KEY (`id_item`)
)
COLLATE='utf8_bin'
ENGINE=InnoDB
;

CREATE TABLE `customers` (
	`id_customer` INT(11) UNSIGNED NOT NULL,
	`name` VARCHAR(200) NULL DEFAULT NULL,
	`email` VARCHAR(320) NULL DEFAULT NULL,
	`credit` DECIMAL(8,2) UNSIGNED NOT NULL DEFAULT '0.00',
	PRIMARY KEY (`id_customer`)
)
COLLATE='utf8_bin'
ENGINE=InnoDB
;

CREATE TABLE `addresses` (
	`id_address` INT(11) UNSIGNED NOT NULL,
	`postal_code` VARCHAR(20) NULL DEFAULT NULL,
	`street` VARCHAR(300) NULL DEFAULT NULL,
	`house` VARCHAR(20) NULL DEFAULT NULL,
	`flat` INT(11) UNSIGNED NULL DEFAULT NULL,
	PRIMARY KEY (`id_address`)
)
COLLATE='utf8_bin'
ENGINE=InnoDB
;

CREATE TABLE `orders` (
	`id_order` INT(11) UNSIGNED NOT NULL,
	`id_customer` INT(11) UNSIGNED NULL DEFAULT NULL,
	`id_address` INT(11) UNSIGNED NULL DEFAULT NULL,
	PRIMARY KEY (`id_order`)
)
COLLATE='utf8_bin'
ENGINE=InnoDB
;

CREATE TABLE `order_1000000001` (
	`id_item` INT(11) UNSIGNED NULL DEFAULT NULL,
	`stock` INT(11) UNSIGNED NOT NULL DEFAULT '0'
)
COLLATE='utf8_bin'
ENGINE=InnoDB
;

CREATE TABLE `order_1000000002` (
	`id_item` INT(11) UNSIGNED NULL DEFAULT NULL,
	`stock` INT(11) UNSIGNED NOT NULL DEFAULT '0'
)
COLLATE='utf8_bin'
ENGINE=InnoDB
;

CREATE TABLE `order_1000000003` (
	`id_item` INT(11) UNSIGNED NULL DEFAULT NULL,
	`stock` INT(11) UNSIGNED NOT NULL DEFAULT '0'
)
COLLATE='utf8_bin'
ENGINE=InnoDB
;

CREATE TABLE `order_1000000004` (
	`id_item` INT(11) UNSIGNED NULL DEFAULT NULL,
	`stock` INT(11) UNSIGNED NOT NULL DEFAULT '0'
)
COLLATE='utf8_bin'
ENGINE=InnoDB
;

CREATE TABLE `order_1000000005` (
	`id_item` INT(11) UNSIGNED NULL DEFAULT NULL,
	`stock` INT(11) UNSIGNED NOT NULL DEFAULT '0'
)
COLLATE='utf8_bin'
ENGINE=InnoDB
;

INSERT INTO `items` (`id_item`, `name`, `cost`, `stock`) VALUES (1000000001, 'Coca-Cola, 0.5 l', 12, 100);
INSERT INTO `items` (`id_item`, `name`, `cost`, `stock`) VALUES (1000000002, 'Coca-Cola, 1 l', 20, 100);
INSERT INTO `items` (`id_item`, `name`, `cost`, `stock`) VALUES (1000000003, 'Coca-Cola, 2 l', 32, 100);
INSERT INTO `items` (`id_item`, `name`, `cost`, `stock`) VALUES (1000000004, 'Fanta, 0.5 l', 12, 100);
INSERT INTO `items` (`id_item`, `name`, `cost`, `stock`) VALUES (1000000005, 'Fanta, 1 l', 20, 100);
INSERT INTO `items` (`id_item`, `name`, `cost`, `stock`) VALUES (1000000006, 'Fanta, 2 l', 32, 100);
INSERT INTO `items` (`id_item`, `name`, `cost`, `stock`) VALUES (1000000007, 'Sprite, 0.5 l', 12, 100);
INSERT INTO `items` (`id_item`, `name`, `cost`, `stock`) VALUES (1000000008, 'Sprite, 1 l', 20, 100);
INSERT INTO `items` (`id_item`, `name`, `cost`, `stock`) VALUES (1000000009, 'Sprite, 2 l', 32, 100);

INSERT INTO `customers` (`id_customer`, `name`, `email`, `credit`) VALUES (1000000001, '������ ������', 'ai@utility.pw', 1000);
INSERT INTO `customers` (`id_customer`, `name`, `email`, `credit`) VALUES (1000000002, '������ �������', 'sergey@utility.pw', 500);
INSERT INTO `customers` (`id_customer`, `name`, `email`, `credit`) VALUES (1000000003, '������� ��������', 'tanya@utility.pw', 700);
INSERT INTO `customers` (`id_customer`, `name`, `email`, `credit`) VALUES (1000000004, '������ �������', 'mixa@utility.pw', 800);
INSERT INTO `customers` (`id_customer`, `name`, `email`, `credit`) VALUES (1000000005, '����� ��������', 'vadim@utility.pw', 900);

INSERT INTO `addresses` (`id_address`, `postal_code`, `street`, `house`, `flat`) VALUES (1000000001, '02068', '���������', '20A', 18);
INSERT INTO `addresses` (`id_address`, `postal_code`, `street`, `house`, `flat`) VALUES (1000000002, '02068', '���������', '20A', 20);
INSERT INTO `addresses` (`id_address`, `postal_code`, `street`, `house`, `flat`) VALUES (1000000003, '02068', '���������', '22', 11);
INSERT INTO `addresses` (`id_address`, `postal_code`, `street`, `house`, `flat`) VALUES (1000000004, '02068', '���������', '24', 240);
INSERT INTO `addresses` (`id_address`, `postal_code`, `street`, `house`, `flat`) VALUES (1000000005, '02068', '���������', '24', 242);
INSERT INTO `addresses` (`id_address`, `postal_code`, `street`, `house`, `flat`) VALUES (1000000006, '02068', '���������', '24', 244);

INSERT INTO `orders` (`id_order`, `id_customer`, `id_address`) VALUES (1000000001, 1000000003, 1000000002);
INSERT INTO `orders` (`id_order`, `id_customer`, `id_address`) VALUES (1000000002, 1000000001, 1000000005);
INSERT INTO `orders` (`id_order`, `id_customer`, `id_address`) VALUES (1000000003, 1000000004, 1000000001);
INSERT INTO `orders` (`id_order`, `id_customer`, `id_address`) VALUES (1000000004, 1000000002, 1000000003);
INSERT INTO `orders` (`id_order`, `id_customer`, `id_address`) VALUES (1000000005, 1000000002, 1000000006);

INSERT INTO `order_1000000001` (`id_item`, `stock`) VALUES (1000000007, 3);
INSERT INTO `order_1000000001` (`id_item`, `stock`) VALUES (1000000008, 4);
INSERT INTO `order_1000000001` (`id_item`, `stock`) VALUES (1000000009, 2);
INSERT INTO `order_1000000002` (`id_item`, `stock`) VALUES (1000000007, 1);
INSERT INTO `order_1000000003` (`id_item`, `stock`) VALUES (1000000002, 2);
INSERT INTO `order_1000000003` (`id_item`, `stock`) VALUES (1000000003, 4);
INSERT INTO `order_1000000004` (`id_item`, `stock`) VALUES (1000000004, 1);
INSERT INTO `order_1000000005` (`id_item`, `stock`) VALUES (1000000002, 5);

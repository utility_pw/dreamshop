-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               10.1.9-MariaDB - mariadb.org binary distribution
-- ОС Сервера:                   Win64
-- HeidiSQL Версия:              9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных dream_shop
CREATE DATABASE IF NOT EXISTS `dream_shop` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `dream_shop`;


-- Дамп структуры для таблица dream_shop.addresses
CREATE TABLE IF NOT EXISTS `addresses` (
  `id_address` int(11) unsigned NOT NULL,
  `postal_code` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `street` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `house` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `flat` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Дамп данных таблицы dream_shop.addresses: ~6 rows (приблизительно)
/*!40000 ALTER TABLE `addresses` DISABLE KEYS */;
INSERT INTO `addresses` (`id_address`, `postal_code`, `street`, `house`, `flat`) VALUES
	(1000000001, '02068', 'Ревуцкого', '20A', 18),
	(1000000002, '02068', 'Ревуцкого', '20A', 20),
	(1000000003, '02068', 'Ревуцкого', '22', 11),
	(1000000004, '02068', 'Ревуцкого', '24', 240),
	(1000000005, '02068', 'Ревуцкого', '24', 242),
	(1000000006, '02068', 'Ревуцкого', '24', 244);
/*!40000 ALTER TABLE `addresses` ENABLE KEYS */;


-- Дамп структуры для таблица dream_shop.customers
CREATE TABLE IF NOT EXISTS `customers` (
  `id_customer` int(11) unsigned NOT NULL,
  `name` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(320) COLLATE utf8_bin DEFAULT NULL,
  `credit` decimal(8,2) unsigned NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id_customer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Дамп данных таблицы dream_shop.customers: ~5 rows (приблизительно)
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` (`id_customer`, `name`, `email`, `credit`) VALUES
	(1000000001, 'Андрей Иванов', 'ai@utility.pw', 1000.00),
	(1000000002, 'Сергей Сергеев', 'sergey@utility.pw', 614.00),
	(1000000003, 'Татьяна Рыжикова', 'tanya@utility.pw', 700.00),
	(1000000004, 'Михаил Романов', 'mixa@utility.pw', 800.00),
	(1000000005, 'Вадим Бегунков', 'vadim@utility.pw', 900.00);
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;


-- Дамп структуры для таблица dream_shop.items
CREATE TABLE IF NOT EXISTS `items` (
  `id_item` int(11) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `cost` decimal(5,2) unsigned NOT NULL DEFAULT '0.00',
  `stock` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_item`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Дамп данных таблицы dream_shop.items: ~9 rows (приблизительно)
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` (`id_item`, `name`, `cost`, `stock`) VALUES
	(1000000001, 'Coca-Cola, 0.5 l', 12.00, 100),
	(1000000002, 'Coca-Cola, 1 l', 20.00, 92),
	(1000000003, 'Coca-Cola, 2 l', 32.00, 84),
	(1000000004, 'Fanta, 0.5 l', 12.00, 100),
	(1000000005, 'Fanta, 1 l', 20.00, 100),
	(1000000006, 'Fanta, 2 l', 32.00, 100),
	(1000000007, 'Sprite, 0.5 l', 12.00, 100),
	(1000000008, 'Sprite, 1 l', 20.00, 100),
	(1000000009, 'Sprite, 2 l', 32.00, 100);
/*!40000 ALTER TABLE `items` ENABLE KEYS */;


-- Дамп структуры для таблица dream_shop.orders
CREATE TABLE IF NOT EXISTS `orders` (
  `id_order` int(11) unsigned NOT NULL,
  `id_customer` int(11) unsigned DEFAULT NULL,
  `id_address` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Дамп данных таблицы dream_shop.orders: ~8 rows (приблизительно)
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` (`id_order`, `id_customer`, `id_address`) VALUES
	(1000000001, 1000000003, 1000000002),
	(1000000002, 1000000001, 1000000005),
	(1000000003, 1000000004, 1000000001),
	(1000000004, 1000000002, 1000000003),
	(1000000005, 1000000002, 1000000006),
	(1000000006, 1000000002, 1000000004),
	(1000000007, 1000000002, 1000000003),
	(1000000008, 1000000002, 1000000004),
	(1000000009, 1000000002, 1000000004);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;


-- Дамп структуры для таблица dream_shop.order_1000000001
CREATE TABLE IF NOT EXISTS `order_1000000001` (
  `id_item` int(11) unsigned DEFAULT NULL,
  `stock` int(11) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Дамп данных таблицы dream_shop.order_1000000001: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `order_1000000001` DISABLE KEYS */;
INSERT INTO `order_1000000001` (`id_item`, `stock`) VALUES
	(1000000007, 3),
	(1000000008, 4),
	(1000000009, 2);
/*!40000 ALTER TABLE `order_1000000001` ENABLE KEYS */;


-- Дамп структуры для таблица dream_shop.order_1000000002
CREATE TABLE IF NOT EXISTS `order_1000000002` (
  `id_item` int(11) unsigned DEFAULT NULL,
  `stock` int(11) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Дамп данных таблицы dream_shop.order_1000000002: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `order_1000000002` DISABLE KEYS */;
INSERT INTO `order_1000000002` (`id_item`, `stock`) VALUES
	(1000000007, 1);
/*!40000 ALTER TABLE `order_1000000002` ENABLE KEYS */;


-- Дамп структуры для таблица dream_shop.order_1000000003
CREATE TABLE IF NOT EXISTS `order_1000000003` (
  `id_item` int(11) unsigned DEFAULT NULL,
  `stock` int(11) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Дамп данных таблицы dream_shop.order_1000000003: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `order_1000000003` DISABLE KEYS */;
INSERT INTO `order_1000000003` (`id_item`, `stock`) VALUES
	(1000000002, 2),
	(1000000003, 4);
/*!40000 ALTER TABLE `order_1000000003` ENABLE KEYS */;


-- Дамп структуры для таблица dream_shop.order_1000000004
CREATE TABLE IF NOT EXISTS `order_1000000004` (
  `id_item` int(11) unsigned DEFAULT NULL,
  `stock` int(11) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Дамп данных таблицы dream_shop.order_1000000004: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `order_1000000004` DISABLE KEYS */;
INSERT INTO `order_1000000004` (`id_item`, `stock`) VALUES
	(1000000004, 1);
/*!40000 ALTER TABLE `order_1000000004` ENABLE KEYS */;


-- Дамп структуры для таблица dream_shop.order_1000000005
CREATE TABLE IF NOT EXISTS `order_1000000005` (
  `id_item` int(11) unsigned DEFAULT NULL,
  `stock` int(11) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Дамп данных таблицы dream_shop.order_1000000005: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `order_1000000005` DISABLE KEYS */;
INSERT INTO `order_1000000005` (`id_item`, `stock`) VALUES
	(1000000002, 5);
/*!40000 ALTER TABLE `order_1000000005` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

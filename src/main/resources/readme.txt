������ � ���������� ������:

...\DreamShop\src\main\java\pw\utility\dreamshop
Address.java - �����.
Customer.java - ����������.
Item.java - �����.
JsonConverter.java - �������������� �������� � JSON-������, � ��������.
ManagementDB.java - ��� �������� � SQL-�����.
Order.java - ���� ������ � ������, �.�. ���� ����� � ��� ����������.
Orders.java - ����������� ������: ID ������ �������, ����������, �����.
RestService.java - ������ � REST � ������ ������.
App.java + JaxRsApplication.java - REST-������.
\DreamShop\src\main\webapp\test.jsp - �������� ���������, ��� ������������ ���������� ������.
DreamShop_NetBeans.zip - ������ ��� IDE NetBeans 8.�
DreamShop_task.txt - ����������� ������� �� ������.
DreamShop_DB_create.txt - ������� �������� ���� � ���������� � ���������� ��������� �������.
DreamShop.sql - ���� �������� ���� ������. ��� ���������� ����� ���������� ������ �� DreamShop_DB_create.txt
readme.txt - ���� ��������


JSON-������� GET:

http://localhost:8080/dreamshop/allitems			��� ������ �� ������.
http://localhost:8080/dreamshop/item/1000000004			���� �� ���������� ������. ����� ����� � ID 1000000004.
http://localhost:8080/dreamshop/allcustomers			��� ����������.
http://localhost:8080/dreamshop/customer/1000000004		���� �� ���������� ����������. ����� ���������� � ID 1000000004.
http://localhost:8080/dreamshop/alladdresses			��� ������.
http://localhost:8080/dreamshop/address/1000000004		���� �� ���������� ������. ����� ����� � ID 1000000004.
http://localhost:8080/dreamshop/allorders			��� ������.
http://localhost:8080/dreamshop/allorderscustomer/1000000002	������ �� ���������� ����������. ����� ���������� � ID 1000000002.
http://localhost:8080/dreamshop/order/1000000002		������ ������� � �� ���������� � ��������� ������. ����� ����� � ID 1000000002.
http://localhost:8080/dreamshop/say/ok				������� �������� ������� �� ���������� ��������. ����� ������ � ����� "ok".


JSON-������ POST:

http://localhost:8080/dreamshop/addorder
�������� ���� ������ �������, ����������, �����.
http://localhost:8084/DreamShop/test.jsp - �������� ���������, ��� ������������ ���������� ������. ����� ���� 8084 ��� Tomcat.
��� �������� ������� �� ���������� ������ ������, ������ ����� ����� � ������� ������ ���� {"new_id_order":1000000008}, ��� � ����� ������ ���� {"new_id_order":3}
1 - �� ������ �� ���������� ������
2 - � ������ ������� ���������� 0
3 - � ������������ ������������ ����� �� �����


������������ ��:

IDE NetBeans 8.1, Java EE 7 Web
�������� ��� ������������ �� ����� Java (JDK) 1.8
Maria DB 10.1 (https://mariadb.org)
Gson  (https://github.com/google/gson)
Maven (https://maven.apache.org/)
jersey (https://jersey.java.net/)


�����������:

* ������������ ������ ���� � ���������� ���������. ������ ������� �� ������ ����������.
* ������ ����� "public Response reAllItems()" ����� ������� ������������, � ��������� �������.
* Json-������ ��� ���������� ������ ����� ����������� ����������� ���������, ��� ��� ���� �������� �� ��������� � ���������.
* ������ �������� � ��������� �� ��� ����� ������, �� ��� ����� ������� �� ����� ��� ����������.
* ���� ������ ������� � �������� ������� �� ��� jetty (https://ru.wikipedia.org/wiki/Jetty)
* ����������� �������� �� �������, � �������� ������� ����� ������������.
* � ����� screenshot ���� �������� � �������.

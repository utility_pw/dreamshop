package pw.utility.dreamshop;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Collator;
import java.util.Locale;

public class Customer implements Comparable {

	private int id_customer;
	private String name;
	private String email;
	private int credit;

    public Customer(ResultSet rs) throws SQLException {
		setID_customer(rs.getInt(1));
		setName(rs.getString(2));
		setEmail(rs.getString(3));
		setCredit(rs.getInt(4));
	}

	public void setID_customer(int id_customer) {
		this.id_customer = id_customer;
	}
	
	public void setName(String name) {
		this.name = name;
    }

	public void setEmail(String email) {
		this.email = email;
	}	

	public void setCredit(int credit) {
		this.credit = credit;
	}
	
    public int getID_customer() {
        return id_customer;
    }

    public String getName() {
        return name;
    }
	
	public String getEmail() {
        return email;
    }

	public int getCredit() {
        return credit;
    }
	
	@Override
    public String toString() {
        return "Customer{" + id_customer + ", " + name + ", " + email + ", " + credit + "}";
    }

    // Метод для сравнения
	@Override
    public int compareTo(Object obj) {
        // Collator - специальный класс для сравнения строк в зависимости от локализации
        Collator c = Collator.getInstance(new Locale("ru"));
        c.setStrength(Collator.PRIMARY);
        return c.compare(this.toString(), obj.toString());
    }
}

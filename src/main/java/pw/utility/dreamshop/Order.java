package pw.utility.dreamshop;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Collator;
import java.util.Locale;

public class Order implements Comparable {

	private int id_item;
	private int stock;

    public Order(ResultSet rs) throws SQLException {
		setID_item(rs.getInt(1));
		setStock(rs.getInt(2));
	}

	public void setID_item(int id_item) {
		this.id_item = id_item;
	}
	
	public void setStock(int stock) {
		this.stock = stock;
    }
	
    public int getID_item() {
        return id_item;
    }

    public int getStock() {
        return stock;
    }
	
	@Override
    public String toString() {
        return "Order{" + id_item + ", " + stock + "}";
    }

    // Метод для сравнения
	@Override
    public int compareTo(Object obj) {
        // Collator - специальный класс для сравнения строк в зависимости от локализации
        Collator c = Collator.getInstance(new Locale("ru"));
        c.setStrength(Collator.PRIMARY);
        return c.compare(this.toString(), obj.toString());
    }
}

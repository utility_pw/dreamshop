package pw.utility.dreamshop;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Collator;
import java.util.Locale;

public class Orders implements Comparable {

	private int id_order;
	private int id_customer;
	private int id_address;

    public Orders(ResultSet rs) throws SQLException {
		setID_order(rs.getInt(1));
		setID_customer(rs.getInt(2));
		setID_address(rs.getInt(3));
	}

	public void setID_order(int id_order) {
		this.id_order = id_order;
	}
	
	public void setID_customer(int id_customer) {
		this.id_customer = id_customer;
    }

	public void setID_address(int id_address) {
		this.id_address = id_address;
	}	
	
    public int getID_order() {
        return id_order;
    }

    public int getID_customer() {
        return id_customer;
    }
	
	public int getID_address() {
        return id_address;
    }
	
	@Override
    public String toString() {
        return "Orders{" + id_order + ", " + id_customer + ", " + id_address + "}";
    }

    // Метод для сравнения
	@Override
    public int compareTo(Object obj) {
        // Collator - специальный класс для сравнения строк в зависимости от локализации
        Collator c = Collator.getInstance(new Locale("ru"));
        c.setStrength(Collator.PRIMARY);
        return c.compare(this.toString(), obj.toString());
    }
}

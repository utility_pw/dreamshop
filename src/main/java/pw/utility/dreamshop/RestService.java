package pw.utility.dreamshop;

import javax.annotation.PostConstruct;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.sql.SQLException;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

@Path("/dreamshop")
public class RestService {
	private ManagementDB mdb = null;
	private JsonConverter ext = null;

    @PostConstruct
    public void init() {
		try {
			mdb = ManagementDB.getInstance();		// Коннект к базе данных
			ext = JsonConverter.getInstance();		// Работа с JSON
		} 
		catch (SQLException e) { Logger.getLogger(RestService.class.getName()).log(Level.SEVERE, null, e); } 
		catch (Exception e) { Logger.getLogger(RestService.class.getName()).log(Level.SEVERE, null, e); }
    }
	
	@GET
    @Path("allitems")								// 	http://localhost:8080/dreamshop/allitems
	@Produces("application/json;charset=UTF-8")
    public Response reAllItems() {
		String result = ext.toJson(mdb.getAllItems()); 
		return Response.status(200).entity(result).build();	
	}
	
	@GET
    @Path("item/{id_item}")							// 	http://localhost:8080/dreamshop/item/1000000004
	@Produces("application/json;charset=UTF-8")
    public Response reItem(@PathParam("id_item") int id_item) {
		String result = ext.toJson(mdb.getItem(id_item));
		return Response.status(200).entity(result).build();
	}
	
	@GET
    @Path("allcustomers")							// 	http://localhost:8080/dreamshop/allcustomers
	@Produces("application/json;charset=UTF-8")
    public Response reAllCustomers() {
		String result = ext.toJson(mdb.getAllCustomers());
		return Response.status(200).entity(result).build();
	}
	
	@GET
    @Path("customer/{id_customer}")					// 	http://localhost:8080/dreamshop/customer/1000000004
	@Produces("application/json;charset=UTF-8")
    public Response reCustomer(@PathParam("id_customer") int id_customer) {
		String result = ext.toJson(mdb.getCustomer(id_customer));
		return Response.status(200).entity(result).build();
	}

	@GET
    @Path("alladdresses")							// 	http://localhost:8080/dreamshop/alladdresses
	@Produces("application/json;charset=UTF-8")
    public Response reAllAddresses() {
		String result = ext.toJson(mdb.getAllAddresses());
		return Response.status(200).entity(result).build();
	}	
	
	@GET
    @Path("address/{id_address}")					// 	http://localhost:8080/dreamshop/address/1000000004
	@Produces("application/json;charset=UTF-8")
    public Response reAddress(@PathParam("id_address") int id_address) {
		String result = ext.toJson(mdb.getAddress(id_address));
		return Response.status(200).entity(result).build();
	}	
	
	@GET
    @Path("allorders")								// 	http://localhost:8080/dreamshop/allorders
	@Produces("application/json;charset=UTF-8")
    public Response reAllOrders() {
		String result = ext.toJson(mdb.getAllOrders());
		return Response.status(200).entity(result).build();
	}	
	
	@GET
    @Path("allorderscustomer/{id_customer}")		// 	http://localhost:8080/dreamshop/allorderscustomer/1000000002
	@Produces("application/json;charset=UTF-8")
    public Response reAllOrdersCustomer(@PathParam("id_customer") int id_customer) {
		String result = ext.toJson(mdb.getAllOrdersCustomer(id_customer));
		return Response.status(200).entity(result).build();
	}	

	@GET
    @Path("order/{id_order}")						// 	http://localhost:8080/dreamshop/order/1000000002
	@Produces("application/json;charset=UTF-8")
    public Response reOrder(@PathParam("id_order") int id_order) {
		String result = ext.toJson(mdb.getOrder(id_order));
		return Response.status(200).entity(result).build();
	}
	
    @POST
    @Path("addorder")								// Тест через http://localhost:8084/dreamshop/test.jsp
	@Produces("application/json;charset=UTF-8")
    public Response queryFormParams(
			@FormParam("jsonorderall") String jsonOrderAll, 
			@FormParam("id_customer") int id_customer, 
			@FormParam("id_address") int id_address){
		Collection<Order> orderAll = ext.fromJsonCollectionOrder(jsonOrderAll);
		int result = mdb.addOrder(orderAll, id_customer, id_address);		
		return Response.status(200).entity("{\"new_id_order\":" + result + "}").build();
    }
	
	// Вариант проверки сервера на готовность отвечать
	@GET
    @Path("say/{something}")						// 	http://localhost:8080/dreamshop/say/ok
	@Produces("application/json;charset=UTF-8")
    public Response saySomething(@PathParam("something") String s){
        return Response.status(200).entity(s).build();
    }
}

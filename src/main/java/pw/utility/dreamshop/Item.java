package pw.utility.dreamshop;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Collator;
import java.util.Locale;

public class Item implements Comparable {

	private int id_item;
	private String name;
	private float cost;
	private int stock;

    public Item(ResultSet rs) throws SQLException {
		setID_item(rs.getInt(1));
		setName(rs.getString(2));
		setCost(rs.getFloat(3));
		setStock(rs.getInt(4));
	}

	public void setID_item(int id_item) {
		this.id_item = id_item;
	}
	
	public void setName(String name) {
		this.name = name;
    }

	public void setCost(float cost) {
		this.cost = cost;
	}	

	public void setStock(int stock) {
		this.stock = stock;
	}
	
    public int getID_item() {
        return id_item;
    }

    public String getName() {
        return name;
    }
	
	public float getCost() {
        return cost;
    }

	public int getStock() {
        return stock;
    }
	
	@Override
    public String toString() {
        return "Item{" + id_item + ", " + name + ", " + cost + ", " + stock + "}";
    }

    // Метод для сравнения
	@Override
    public int compareTo(Object obj) {
        // Collator - специальный класс для сравнения строк в зависимости от локализации
        Collator c = Collator.getInstance(new Locale("ru"));
        c.setStrength(Collator.PRIMARY);
        return c.compare(this.toString(), obj.toString());
    }
}

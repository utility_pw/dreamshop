package pw.utility.dreamshop;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Collator;
import java.util.Locale;

public class Address implements Comparable {

	private int id_address;
	private String postal_code;
	private String street;
	private String house;
	private int flat;

    public Address(ResultSet rs) throws SQLException {
		setID_address(rs.getInt(1));
		setPostal_code(rs.getString(2));
		setStreet(rs.getString(3));
		setHouse(rs.getString(4));
		setFlat(rs.getInt(5));
	}

	public void setID_address(int id_address) {
		this.id_address = id_address;
	}
	
	public void setPostal_code(String postal_code) {
		this.postal_code = postal_code;
    }

	public void setStreet(String street) {
		this.street = street;
	}	

	public void setHouse(String house) {
		this.house = house;
	}
		
	public void setFlat(int flat) {
		this.flat = flat;
	}
	
    public int getID_address() {
        return id_address;
    }

    public String getPostal_code() {
        return postal_code;
    }
	
	public String getStreet() {
        return street;
    }

	public String getHouse() {
        return house;
    }
		
	public int getFlat() {
        return flat;
    }
	
	@Override
    public String toString() {
        return "Address{" + id_address + ", " + postal_code + ", " + street + ", " + house + ", " + flat + "}";
    }

    // Метод для сравнения
	@Override
    public int compareTo(Object obj) {
        // Collator - специальный класс для сравнения строк в зависимости от локализации
        Collator c = Collator.getInstance(new Locale("ru"));
        c.setStrength(Collator.PRIMARY);
        return c.compare(this.toString(), obj.toString());
    }
}

package pw.utility.dreamshop;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.Collection;

public class JsonConverter {
    private static JsonConverter instance;
	private static Gson gson = new Gson();
	
    private JsonConverter() throws Exception {
    }

    public static synchronized JsonConverter getInstance() throws Exception {
        if (instance == null) { instance = new JsonConverter(); }
        return instance;
    }

	public String toJson(Object obj){
		String json = gson.toJson(obj); 
		return json;
    }

	public String toJson(Collection<Order> obj){
		String json0 = gson.toJson(obj); 
		Type collectionType = new TypeToken<Collection<Order>>(){}.getType();
		Collection<Order> obj2 = gson.fromJson(json0, collectionType);
		String json = gson.toJson(obj2);
		return json;
    }
	
	public Item fromJsonItem(String str){
		Item result = gson.fromJson(str, Item.class);
		return result;
    }
		
	public Collection<Order> fromJsonCollectionOrder(String str){
		Type collectionType = new TypeToken<Collection<Order>>(){}.getType();
		Collection<Order> result = gson.fromJson(str, collectionType);
		return result;
    }
				
}

package pw.utility.dreamshop;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ManagementDB {
	private static Connection connect;
    private static ManagementDB instance;

    private ManagementDB() {
        try {
			Class.forName("org.mariadb.jdbc.Driver");
            String url = "jdbc:mariadb://localhost:3306/dream_shop";
            connect = DriverManager.getConnection(url, "root", "root");
        } 
		catch (ClassNotFoundException e) { myLog(e); }
		catch (SQLException e) { myLog(e); } 
		catch (Exception e) { myLog(e); }
    }

    public static synchronized ManagementDB getInstance() throws Exception {
        if (instance == null) { instance = new ManagementDB(); }
        return instance;
    }
	
	// Получение коллекции товаров
	public Collection<Item> getAllItems() {
		Collection<Item> itemsAll = new ArrayList<>();
        Statement stmt = null;
        ResultSet rs = null;
		Item st = null;
        try {
            stmt = connect.createStatement();
			rs = stmt.executeQuery("SELECT * FROM items");
            while (rs.next()) {
                st = new Item(rs);
                itemsAll.add(st);
            }
        } 
		catch (SQLException e) { myLog(e); }
		catch (Exception e) { myLog(e); }
		finally {
            if (rs != null) { 
				try { rs.close(); } 
				catch (SQLException e) { myLog(e); }
			}
            if (stmt != null) { 
				try { stmt.close(); } 
				catch (SQLException e) { myLog(e); }
			}
        }
        return itemsAll;
    }

	// Получение товара по его ID
	public Item getItem(int id) {
        Statement stmt = null;
        ResultSet rs = null;
		Item st = null;
        try {
            stmt = connect.createStatement();
            rs = stmt.executeQuery("SELECT * FROM items WHERE id_item = " + id);
            if (rs.next()) { st = new Item(rs); }
        } 
		catch (SQLException e) { myLog(e); }
		catch (Exception e) { myLog(e); }
		finally {
            if (rs != null) { 
				try { rs.close(); } 
				catch (SQLException e) { myLog(e); }
			}
            if (stmt != null) { 
				try { stmt.close(); } 
				catch (SQLException e) { myLog(e); }
			}
        }
        return st;
    }
	
	// Получение коллекции покупателей
	public Collection<Customer> getAllCustomers() {
		Collection<Customer> сustomersAll = new ArrayList<>();
        Statement stmt = null;
        ResultSet rs = null;
		Customer st = null;
        try {
            stmt = connect.createStatement();
			rs = stmt.executeQuery("SELECT * FROM customers");
            while (rs.next()) {
                st = new Customer(rs);
                сustomersAll.add(st);
            }
        } 
		catch (SQLException e) { myLog(e); }
		catch (Exception e) { myLog(e); }
		finally {
            if (rs != null) { 
				try { rs.close(); } 
				catch (SQLException e) { myLog(e); }
			}
            if (stmt != null) { 
				try { stmt.close(); } 
				catch (SQLException e) { myLog(e); }
			}
        }
        return сustomersAll;
    }
	
	// Получение покупателя по его ID
	public Customer getCustomer(int id) {
        Statement stmt = null;
        ResultSet rs = null;
		Customer st = null;
        try {
            stmt = connect.createStatement();
            rs = stmt.executeQuery("SELECT * FROM customers WHERE id_customer = " + id);
            if (rs.next()) { st = new Customer(rs); }
        } 
		catch (SQLException e) { myLog(e); }
		catch (Exception e) { myLog(e); }
		finally {
            if (rs != null) { 
				try { rs.close(); } 
				catch (SQLException e) { myLog(e); }
			}
            if (stmt != null) { 
				try { stmt.close(); } 
				catch (SQLException e) { myLog(e); }
			}
        }
        return st;
    }	
	
	// Получение коллекции адресов
	public Collection<Address> getAllAddresses() {
		Collection<Address> AddressAll = new ArrayList<>();
        Statement stmt = null;
        ResultSet rs = null;
		Address st = null;
        try {
            stmt = connect.createStatement();
			rs = stmt.executeQuery("SELECT * FROM addresses");
            while (rs.next()) {
                st = new Address(rs);
                AddressAll.add(st);
            }
        } 
		catch (SQLException e) { myLog(e); }
		catch (Exception e) { myLog(e); }
		finally {
            if (rs != null) { 
				try { rs.close(); } 
				catch (SQLException e) { myLog(e); }
			}
            if (stmt != null) { 
				try { stmt.close(); } 
				catch (SQLException e) { myLog(e); }
			}
        }
        return AddressAll;
    }

	// Получение адреса по его ID
	public Address getAddress(int id) {
        Statement stmt = null;
        ResultSet rs = null;
		Address st = null;
        try {
            stmt = connect.createStatement();
            rs = stmt.executeQuery("SELECT * FROM addresses WHERE id_address = " + id);
            if (rs.next()) { st = new Address(rs); }
        } 
		catch (SQLException e) { myLog(e); }
		catch (Exception e) { myLog(e); }
		finally {
            if (rs != null) { 
				try { rs.close(); } 
				catch (SQLException e) { myLog(e); }
			}
            if (stmt != null) { 
				try { stmt.close(); } 
				catch (SQLException e) { myLog(e); }
			}
        }
        return st;
    }
	
	// Получение коллекции заказов по всем покупателям
	public Collection<Orders> getAllOrders() {
		Collection<Orders> ordersAll = new ArrayList<>();
        Statement stmt = null;
        ResultSet rs = null;
		Orders st = null;
        try {
            stmt = connect.createStatement();
			rs = stmt.executeQuery("SELECT * FROM orders");
            while (rs.next()) {
                st = new Orders(rs);
                ordersAll.add(st);
            }
        } 
		catch (SQLException e) { myLog(e); }
		catch (Exception e) { myLog(e); }
		finally {
            if (rs != null) { 
				try { rs.close(); } 
				catch (SQLException e) { myLog(e); }
			}
            if (stmt != null) { 
				try { stmt.close(); } 
				catch (SQLException e) { myLog(e); }
			}
        }
        return ordersAll;
    }

	// Получение коллекции заказов по одному покупателю
	public Collection<Orders> getAllOrdersCustomer(int id) {
		Collection<Orders> ordersAll = new ArrayList<>();
        Statement stmt = null;
        ResultSet rs = null;
		Orders st = null;
        try {
            stmt = connect.createStatement();
			rs = stmt.executeQuery("SELECT * FROM orders WHERE id_customer = " + id);
            while (rs.next()) {
                st = new Orders(rs);
                ordersAll.add(st);
            }
        } 
		catch (SQLException e) { myLog(e); }
		catch (Exception e) { myLog(e); }
		finally {
            if (rs != null) { 
				try { rs.close(); } 
				catch (SQLException e) { myLog(e); }
			}
            if (stmt != null) { 
				try { stmt.close(); } 
				catch (SQLException e) { myLog(e); }
			}
        }
        return ordersAll;
    }
	
	// Получение заказа по его ID
	public Collection<Order> getOrder(int id) {
		Collection<Order> orderAll = new ArrayList<>();
        Statement stmt = null;
        ResultSet rs = null;
		Order st = null;
        try {
            stmt = connect.createStatement();
			rs = stmt.executeQuery("SELECT * FROM order_" + id);
            while (rs.next()) {
                st = new Order(rs);
                orderAll.add(st);
            }
        } 
		catch (SQLException e) { myLog(e); }
		catch (Exception e) { myLog(e); }
		finally {
            if (rs != null) { 
				try { rs.close(); } 
				catch (SQLException e) { myLog(e); }
			}
            if (stmt != null) { 
				try { stmt.close(); } 
				catch (SQLException e) { myLog(e); }
			}
        }
        return orderAll;
    }	

	// Добавление нового заказа
	public synchronized int addOrder(Collection<Order> orderAll, int id_customer , int id_address) {
		float sum = 0;
		String [] ins = new String[orderAll.size()];	// добавление товаров в таблицу нового заказа
		String [] modItem = new String[orderAll.size()];	// изменение количества товаров на складе
		int id_order = 0;
		// Проверка на количество денег
		Statement stmt = null;
        ResultSet rs = null;
		float creditDB = 0;								// по-умолчанию кредит пуст
        try {
			stmt = connect.createStatement();
			rs = stmt.executeQuery("SELECT credit FROM customers WHERE id_customer = " + id_customer);
			while (rs.next()) { creditDB = rs.getFloat(1); }				// количество денег на счету пользователя
			rs = stmt.executeQuery("SELECT `id_order` FROM `orders` WHERE `id_order` = (SELECT MAX(`id_order`) FROM `orders`);");
			while (rs.next()) { id_order = rs.getInt(1) + 1; }
		} 
		catch (SQLException e) { myLog(e); }
		catch (Exception e) { myLog(e); }
		finally {
            if (rs != null) { 
				try { rs.close(); } 
				catch (SQLException e) { myLog(e); }
			}
            if (stmt != null) { 
				try { stmt.close(); } 
				catch (SQLException e) { myLog(e); }
			}
        }
		
		// Проверка на количество товара
		for (int i=0; i < orderAll.size(); i++) {
			ArrayList<Order> ob = new ArrayList<>(orderAll);
			Order obIN = ob.get(i);						// извлекаю объект из коллекции
			int stock = obIN.getStock();
			int id_item = obIN.getID_item();
			stmt = null;
			rs = null;
			int stockDB = 0;
			float costDB = 0;
			try {
				stmt = connect.createStatement();
				rs = stmt.executeQuery("SELECT stock, cost FROM items WHERE id_item = " + id_item);
				while (rs.next()) {
					stockDB = rs.getInt(1);				// количество товара на складе
					costDB = rs.getFloat(2);			// стоимость единицы товара
				}
			} 
		catch (SQLException e) { myLog(e); }
		catch (Exception e) { myLog(e); }
		finally {
            if (rs != null) { 
				try { rs.close(); } 
				catch (SQLException e) { myLog(e); }
			}
            if (stmt != null) { 
				try { stmt.close(); } 
				catch (SQLException e) { myLog(e); }
			}
        }
			if ((stockDB-stock) < 0) {return 1;}		// на складе не достаточно товара
			if (stock == 0) {return 2;}					// в заказе указано количество 0		
			sum = sum + (costDB * stock);				// текущая сумма заказа
			if (sum > creditDB) {return 3;}				// у пользователя недостаточно денег на счету
			ins[i] = "INSERT INTO `order_" + id_order + "` (`id_item`, `stock`) VALUES (" + id_item + ", " + stock + ");";
			modItem[i] = "UPDATE items SET stock=" + (stockDB-stock) + " WHERE id_item=" + id_item + ";";
		}
		
		// Запись заказа в базу 
        stmt = null;
        try {
            stmt = connect.createStatement();
			stmt.execute("CREATE TABLE `order_" + id_order + "` (`id_item` INT(11) UNSIGNED NULL DEFAULT NULL, `stock` INT(11) UNSIGNED NOT NULL DEFAULT '0') COLLATE='utf8_bin' ENGINE=InnoDB;");
			for (String item : ins) { stmt.execute(item); }		// добавление товаров в таблицу заказа
			// Добавление информации о заказе в общий список заказов
			stmt.execute("INSERT INTO `orders` (`id_order`, `id_customer`, `id_address`) VALUES (" + id_order + ", " + id_customer + ", " + id_address + ");");
			// Убавить в базе количество товаров
			for (String item : modItem) { stmt.execute(item); }
			// Убавить в базе количество денег на счёте
			stmt.execute("UPDATE customers SET credit=" + (creditDB-sum) + " WHERE id_customer=" + id_customer + ";");
		} 
		catch (SQLException e) { myLog(e); }
		catch (Exception e) { myLog(e); }
		finally {
            if (stmt != null) { 
				try { stmt.close(); } 
				catch (SQLException e) { myLog(e); }
			}
        }
		return id_order;
    }
	
	public final void myLog(Exception ex) {
		Logger.getLogger(ManagementDB.class.getName()).log(Level.SEVERE, null, ex);
		ex.printStackTrace();
	}
}
